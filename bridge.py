#!/usr/bin/python2
from fish_show import FishShow
from sunrise import SunriseShow
from constants import *

try:
    import lumiversepython
except ImportError:
    have_lumiversepython = False
    from DummyRig import DummyRig
else:
    have_lumiversepython = True

class Bridge(object):
    WIDTH = BRIDGE_WIDTH
    HEIGHT = BRIDGE_HEIGHT

    def __init__(self):
        if have_lumiversepython:
            self.rig = lumiversepython.Rig('/home/teacher/Lumiverse/PBridge.rig.json')
        else:
            self.rig = DummyRig(self.WIDTH, self.HEIGHT)
        self.rig.init()
        self.lights = self.get_lights()

    def update(self):
        self.rig.updateOnce()

    def get_light(self, x, y):
        return self.lights[x][y]

    def get_lights(self):
        return [ [ self.rig.select('$side={y}[$sequence={x}]'.format(y=y, x=x))
                   for y in ('bot', 'top') ]
                 for x in xrange(1, self.WIDTH+1)]


def main():
    """Just for testing. Use ShowRunner when running the show for real."""
    import Queue
    bridge = Bridge()
    show = FishShow(bridge, send_queue=Queue.Queue(), recv_queue=Queue.Queue())
    #show = SunriseShow(bridge)
    show.run(framerate=FRAME_RATE)

if __name__ == '__main__':
    main()
