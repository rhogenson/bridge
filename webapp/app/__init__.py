from flask import Flask
import socketio

import Queue

recv_queue = Queue.Queue()
send_queue = Queue.Queue()

sio = socketio.Server(async_mode='threading')
app = Flask(__name__)
app.wsgi_app = socketio.Middleware(sio, app.wsgi_app)
from app import views


