from flask import render_template, request, send_from_directory
from app import app, sio, send_queue, recv_queue
import time

import threading

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html',
                           title='F16 Group1')

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

connections = set()

@sio.on('connect', namespace='/music')
def connect_music(sid, environ):
    print("connect_music", sid)

@sio.on('disconnect', namespace='/music')
def disconnect_music(sid):
    print("disconnect_music", sid)

current_background_music = None
@sio.on('ready', namespace='/music')
def ready_music(sid):
    sio.emit('background', {'id': current_background_music}, room=sid, namespace='/music')

@sio.on('connect', namespace='/spawn')
def connect(sid, environ):
    print("connect ", sid)
    connections.add(sid)

@sio.on('spawn', namespace='/spawn')
def message(sid, data):
    print("message ", data)
    send_queue.put(('spawn', data))
    sio.emit('effect', {'id': data['type']}, namespace='/music');

@sio.on('ready', namespace='/spawn')
def ready(sid):
    sio.emit('fish state', fish_state, room=sid, namespace='/spawn')

@sio.on('disconnect', namespace='/spawn')
def disconnect(sid):
    print('disconnect', sid)
    connections.remove(sid)

fish_state = {
        'jellyfish': 0,
        'dolphin': 0,
        'boat': 0,
        'nemo': 0,
        'dory': 0,
        'whale': 0,
        'eel': 0,
        'stingray': 0,
        'shark': 0
}

def recv_events():
    while True:
        event = recv_queue.get()
        if event[0] == 'enable image':
            fish_state[event[1]['name']] = 1
            sio.emit('fish state', fish_state, namespace='/spawn')
        elif event[0] == 'disable image':
            fish_state[event[1]['name']] = 0
            sio.emit('fish state', fish_state, namespace='/spawn')
        elif event[0] == 'background music':
            print "****** background music ", event[1]
            sio.emit('background', {'id': event[1]}, namespace='/music');
            current_background_music = event[1];
        elif event[0] == 'effect music':
            sio.emit('effect', {'id': event[1]}, namespace='/music');



thread = sio.start_background_task(recv_events)
