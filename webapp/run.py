#!/usr/bin/env python

import sys
sys.path.append('webapp/packages')

import threading

from app import app, send_queue, recv_queue

def run_server():
    app.run(threaded=True, host='', port=8000)

server_thread = threading.Thread(target=run_server)
