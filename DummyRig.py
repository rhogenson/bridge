import pygame
from pygame.locals import *
import re
import threading
import time

class DummyLight(object):
    WIDTH_PX = 8
    HEIGHT_PX = 20

    def __init__(self):
        self.color = (0, 0, 0)

    def setRGBRaw(self, r, g, b):
        self.color = (int(r*255), int(g*255), int(b*255))


class DummyRig(object):
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.margin = 5

    def init(self):
        pygame.init()
        self.screen = pygame.display.set_mode((self.width*DummyLight.WIDTH_PX + self.margin*2,
                                               self.height*DummyLight.HEIGHT_PX + self.margin * 2))
        self.bg = pygame.Surface(self.screen.get_size())
        self.bg = self.bg.convert()
        self.bg.fill((0, 0, 0))
        self.screen.blit(self.bg, (0, 0))
        pygame.display.flip()

        self.lights = [[DummyLight() for _ in xrange(self.height)]
                       for _ in xrange(self.width)]

    def select(self, pattern):
        match = re.search("\$side=(.*?)\[\$sequence=(.*?)\]", pattern)
        if match.group(1) == "bot":
            y = 1
        elif match.group(1) == "top":
            y = 0

        x = int(match.group(2)) - 1
        return self.lights[x][y]

    def updateOnce(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                raise Exception

        self.screen.blit(self.bg, (0,0))
        self.drawRects()
        pygame.display.flip()

    def drawRects(self):
        for x in xrange(self.width):
            for y in xrange(self.height):
                light = self.lights[x][y]
                x0 = self.margin + x * DummyLight.WIDTH_PX
                y0 = self.margin + y * DummyLight.HEIGHT_PX
                pygame.draw.rect(self.screen, light.color,
                                 (x0, y0, DummyLight.WIDTH_PX, DummyLight.HEIGHT_PX))
