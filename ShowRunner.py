from bridge import Bridge
from fish_show import FishShow
from sunrise import SunriseShow
from twinkle import TwinkleShow
from constants import *

import sys
sys.path.append('webapp')
from run import server_thread, send_queue, recv_queue

def run_show():
    server_thread.start()
    bridge = Bridge()

    print "Press enter to begin show"
    x = raw_input()

    TwinkleShow(bridge, send_queue=recv_queue, recv_queue=send_queue).run(framerate=FRAME_RATE)
    SunriseShow(bridge, send_queue=recv_queue, recv_queue=send_queue).run(framerate=FRAME_RATE)
    FishShow(bridge, send_queue=recv_queue, recv_queue=send_queue).run(framerate=FRAME_RATE)
    print "done!"

run_show()
