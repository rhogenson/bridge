"""Constants that may be tweaked in the future."""
from __future__ import division, unicode_literals

FRAME_RATE = 22
MIN_VALUE = 0
MAX_VALUE = 1
MAX_TIME = 2
MIN_TIME = 0.1
MIN_HUE = 207 / 360
MAX_HUE = 250 / 360
MAX_SATURATION = 1
MIN_SATURATION = 0.5
MIN_VALUE = 0.3
MAX_VALUE = 1

# shallow
MIN_HUE = 169 / 360
MAX_HUE = 250 / 360
MAX_SATURATION = 1
MIN_SATURATION = 0.5
MIN_VALUE = 0.3
MAX_VALUE = 1

BRIDGE_WIDTH = 200
BRIDGE_HEIGHT = 2
