from __future__ import division
import color
from color import Vector
import random
import constants
from constants import *
import math
from show import Show
from fish_types import Fish


class Light(object):
    """Represent a bridge light and its state."""
    __slots__=('light', 'color', 'transitioning_to', 'x', 'y', 'step', 'result_color', 'fade_step')
    def __init__(self, x, y, light):
        """Store all facts about a light."""
        self.light = light
        self.color = color.black
        self.result_color = self.color
        self.transitioning_to = self.color
        self.step = 0
        self.x = x
        self.y = y
        self.fade_step = 0

    def update_base(self, depth):
        """Change color depending on where the fish is."""
        if abs(self.transitioning_to - self.color) <= self.step:
            self.transitioning_to = pick_HSV(depth).to_RGB()
            self.step = (random_change()
                * abs(self.color - self.transitioning_to))
        new_background = transition(
            self.color, self.transitioning_to, self.step
        )

        self.color = new_background
        self.result_color = new_background

    def update_fish(self, fish):
        coverage = compute_coverage(fish, self.x, self.y)
        fish_color = fish.color_at(self.x, self.y)
        self.result_color = coverage * fish_color + (1 - coverage) * self.result_color

    def blit(self):
        self.light.setRGBRaw(
            *map(lambda x: clamp(0, x, 1), self.result_color.components)
        )

    def fade_to_black(self, steps):
        hue = self.color.to_HSV().h()
        sat = self.color.to_HSV().s()
        value = self.color.to_HSV().v()
        new_color = color.HSV(hue, sat, value - value / steps * self.fade_step).to_RGB()
        self.fade_step += 1
        self.light.setRGBRaw(*map(lambda x: clamp(0, x, 1), new_color.components))
        

def compute_coverage(fish, x, y):
    """Calculate the coverage of a fish over a box."""
    middle_of_fish = fish.x + fish.width / 2
    middle_of_panel = x + 0.5
    diff = abs(middle_of_fish - middle_of_panel) / (fish.width / 2)
    scale = 1 - diff
    min_x = min(max(x, fish.x), x + 1)
    max_x = max(min(x+1, fish.x + fish.width), x)
    min_y = min(max(y, fish.y), y + 1)
    max_y = max(min(y+1, fish.y + fish.height), y)
    coverage = (max_x - min_x) * (max_y - min_y)
    if fish.is_diamond:
        return coverage * scale
    return coverage * fish.alpha_at(x, y)


def pick_HSV(depth):
    """Return a random color in an appropriate interval."""
    min_hue = constants.MIN_HUE + (10/360)*depth
    min_saturation = constants.MIN_SATURATION + 0.05*depth
    hue = random.random() * (constants.MAX_HUE - min_hue) + min_hue
    saturation = random.random() * (MAX_SATURATION - min_saturation) + min_saturation
    value = random.random() * (MAX_VALUE - MIN_VALUE) + MIN_VALUE
    return color.HSV(hue, saturation, value)


def transition(start, end, step):
    """Return a vector equal to (end - start) * step + start."""
    if start == end:
        return start
    diff_vector = end - start
    step_vector = diff_vector / abs(diff_vector) * step
    result = step_vector + start
    return result


def random_change():
    """Return a random amount that a light should change each frame."""
    min_time = int(constants.MIN_TIME * FRAME_RATE)
    max_time = int(MAX_TIME * FRAME_RATE)
    return 1 / random.randint(min_time, max_time)


def clamp(low, x, high):
    """Return v such that low <= v <= high and |x - v| is minimal."""
    return max(low, min(x, high))

class FishShow(Show):
    # 1 minute show
    TICKS_PER_DEPTH = FRAME_RATE * 75
    MAX_DEPTH = 6

    def init(self):
        self.lights = [[Light(x, y, self.bridge.get_light(x, y))
                       for y in xrange(self.bridge.HEIGHT)]
                       for x in xrange(self.bridge.WIDTH)]
        self.fishes = set()
        self.tick_count = 0
        self.started_fade_tick = None
        self.depth = 0
        self.send_event('background music', 'underwater');

        self.send_event('disable image', {'name': 'jellyfish'})
        self.send_event('disable image', {'name': 'dolphin'})
        self.send_event('disable image', {'name': 'boat'})
        self.send_event('disable image', {'name': 'nemo'})
        self.send_event('disable image', {'name': 'dory'})
        self.send_event('disable image', {'name': 'whale'})
        self.send_event('disable image', {'name': 'eel'})
        self.send_event('disable image', {'name': 'stingray'})
        self.send_event('disable image', {'name': 'shark'})

    def update(self):
        event = self.recv_event()
        if self.depth <= self.MAX_DEPTH:
            if event and event[0] == 'spawn':
                new_fish = Fish.by_type(event[1]['type'])
                if new_fish:
                    self.fishes.add(new_fish)

            for col in self.lights:
                for light in col:
                    light.update_base(self.depth)

            for fish in list(self.fishes):
                if not fish.update():
                    self.fishes.remove(fish)

                for x in xrange(int(fish.x), int(fish.x+fish.width+1)):
                    if x < len(self.lights):
                        for light in self.lights[x]:
                            light.update_fish(fish)

            for col in self.lights:
                for light in col:
                    light.blit()
            
            if self.depth == 0:
                    self.send_event('enable image', {'name': 'boat'})
                    self.send_event('enable image', {'name': 'dolphin'})
                    self.send_event('enable image', {'name': 'jellyfish'})

            self.tick_count += 1
            if self.tick_count % self.TICKS_PER_DEPTH == 0:
                self.depth += 1
                print("DEPTH: ", self.depth)
                if self.depth == 1:
                    self.send_event('enable image', {'name': 'dory'})
                    self.send_event('disable image', {'name': 'boat'})
                elif self.depth == 2:
                    self.send_event('disable image', {'name': 'dolphin'})
                    self.send_event('enable image', {'name': 'nemo'})
                    self.send_event('enable image', {'name': 'whale'})
                elif self.depth == 3:
                    self.send_event('disable image', {'name': 'jellyfish'})
                    self.send_event('enable image', {'name': 'shark'})
                elif self.depth == 4:
                    self.send_event('disable image', {'name': 'nemo'})
                    self.send_event('disable image', {'name': 'dory'})
                    self.send_event('enable image', {'name': 'stingray'})
                elif self.depth == 5:
                    self.send_event('enable image', {'name': 'eel'})
                elif self.depth == 6:
                    self.send_event('disable image', {'name': 'whale'})
                    self.send_event('disable image', {'name': 'shark'})
                else:
                    self.send_event('disable image', {'name': 'stingray'})
                    self.send_event('disable image', {'name': 'eel'})

        else:
            fade_time = 5 # sec
            if self.started_fade_tick is None:
                self.started_fade_tick = self.tick_count
            if self.tick_count > self.started_fade_tick + FRAME_RATE * fade_time:
                self.stop()
                return
            for col in self.lights:
                for light in col:
                    light.fade_to_black(fade_time * FRAME_RATE)
            
