"""Rendering classes and helper functions."""
from __future__ import division
import color
import random
import constants
from constants import *
import math
from show import Show

class Sun(object):
    """Object displayed on the bridge, probably a fish."""
    def __init__(self):
        """Create fish with default parameters."""
        self.x = 25
        self.y = 0 - 1
        self.width = 150
        self.height = 2
        self.base_color = color.HSV(27/360, 1, 0.5)
        self.end_color = color.HSV(53/360, 1, 1)
        self.color = self.base_color.to_RGB()

    def update(self):
        """Swim gently to the other side of the bridge."""
        self.y += 0.003
        if self.y + self.height >= 3:
            self.y = 3 - self.height
        elif self.y + self.height > 1:
            self.width += 0.2
            self.x -= 0.1
            color_range = (self.end_color - self.base_color).unit()
            self.color = (self.y + self.height - 1) / 3 * color_range + self.base_color
            self.color = self.color.to_RGB()

        #print(self.y)

    #def color_at(self, x, y):
        #ball_width = 15
        #if abs(x - self.center_x) < ball_width:
            #return color.RGB(1, 1, 1)
        #distance = abs(self.center_x - x)
        #start_hue = 58 / 360
        #end_hue = 41 / 360
        #start_sat = 0
        #end_sat = 0.92
        #sat = scale_with_clamp(distance, ball_width, 60, start_sat, end_sat)
        #hue = scale_with_clamp(distance, ball_width, 100, start_hue, end_hue)
        #return color.HSV(hue, sat, 1).to_RGB()


def scale_with_clamp(inp, min_inp, max_inp, min_out, max_out):
    return (clamp(min_inp, inp, max_inp) - min_inp) / (max_inp - min_inp) * (max_out - min_out) + min_out


class Light(object):
    """Represent a bridge light and its state."""
    def __init__(self, x, y, light):
        """Store all facts about a light."""
        self.light = light
        self.color = color.black
        self.transitioning_to = self.color
        self.step = 0
        self.x = x
        self.y = y
        self.fade_step = 0

    def update(self, sky_brightness, sun):
        """Change color depending on where the fish is."""
        if self.y == 0:
            if abs(self.transitioning_to - self.color) <= self.step:
                self.transitioning_to = pick_ocean_HSV(sky_brightness).to_RGB()
                self.step = (random_change()
                    * abs(self.color - self.transitioning_to))
            self.color = transition(
                self.color, self.transitioning_to, self.step
            )
            self.light.setRGBRaw(
                *map(lambda x: clamp(0, x, 1), self.color.components)
            )
        else:
            sky_color = color.HSV(250/360, 0.25, sky_brightness).to_RGB()
            coverage = compute_coverage(sun, self.x, self.y)
            self.color = coverage * sun.color + (1 - coverage) * sky_color
            self.light.setRGBRaw(
                *map(lambda x: clamp(0, x, 1), self.color.components)
            )

    def fade_to_black(self, steps):
        hue = self.color.to_HSV().h()
        sat = self.color.to_HSV().s()
        value = self.color.to_HSV().v()
        new_color = color.HSV(hue, sat, value - value / steps * self.fade_step).to_RGB()
        self.fade_step += 1
        self.light.setRGBRaw(*map(lambda x: clamp(0, x, 1), new_color.components))


class SunriseShow(Show):
    def init(self):
        self.lights = [Light(x, y, self.bridge.get_light(x, y))
                       for y in xrange(self.bridge.HEIGHT)
                       for x in xrange(self.bridge.WIDTH)]
        self.sun = Sun()
        self.sky_brightness = 0
        self.tick_count = 0
        self.send_event('background music', 'seashore')


    def update(self):
        if self.tick_count > FRAME_RATE * 90:
            fade_time = 3 # sec
            if self.tick_count > FRAME_RATE * 90 + fade_time * FRAME_RATE:
                self.stop()
                return
            for light in self.lights:
                light.fade_to_black(fade_time * FRAME_RATE)
        else:
            if self.sky_brightness >= 1:
                self.sun.update()
            for light in self.lights:
                light.update(self.sky_brightness, self.sun)

            if self.sky_brightness < 1:
                self.sky_brightness += 0.03 / FRAME_RATE

        self.tick_count += 1


def compute_coverage(fish, x, y):
    """Calculate the coverage of a fish over a box."""
    middle_of_fish = fish.x + fish.width / 2
    middle_of_panel = x + 0.5
    diff = abs(middle_of_fish - middle_of_panel) / (fish.width / 2)
    scale = 1 - diff
    min_x = min(max(x, fish.x), x + 1)
    max_x = max(min(x+1, fish.x + fish.width), x)
    min_y = min(max(y, fish.y), y + 1)
    max_y = max(min(y+1, fish.y + fish.height), y)
    coverage = (max_x - min_x) * (max_y - min_y)
    return coverage * scale


def pick_ocean_HSV(sky_brightness):
    """Return a random color in an appropriate interval."""
    min_hue = constants.MIN_HUE
    min_saturation = constants.MIN_SATURATION
    hue = random.random() * (constants.MAX_HUE - min_hue) + min_hue
    saturation = random.random() * (MAX_SATURATION - min_saturation) + min_saturation
    return color.HSV(hue, saturation, sky_brightness)


def transition(start, end, step):
    """Return a vector equal to (end - start) * step + start."""
    if start == end:
        return start
    diff_vector = end - start
    step_vector = diff_vector / abs(diff_vector) * step
    result = step_vector + start
    return result


def random_change():
    """Return a random amount that a light should change each frame."""
    min_time = int(constants.MIN_TIME * FRAME_RATE)
    max_time = int(MAX_TIME * FRAME_RATE)
    return 1 / random.randint(min_time, max_time)


def clamp(low, x, high):
    """Return v such that low <= v <= high and |x - v| is minimal."""
    return max(low, min(x, high))
