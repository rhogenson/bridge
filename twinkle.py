"""Rendering classes and helper functions."""
from __future__ import division
import color
import random
import constants
from constants import *
import math
from show import Show

class ShootingStar(object):
    """Object displayed on the bridge, probably a fish."""
    def __init__(self):
        """Create fish with default parameters."""
        self.x = random.random() * BRIDGE_WIDTH
        self.y = random.random() * 1.5
        self.width = 5
        self.height = 0.5
        self.color = color.RGB(1, 1, 1)
        if random.random() >= 0.5:
            self.speed = 8
        else:
            self.speed = -8

    def update(self):
        self.x += self.speed
        self.speed *= 0.9
        return abs(self.speed) > 1 and self.x < BRIDGE_WIDTH


class Light(object):
    """Represent a bridge light and its state."""
    def __init__(self, x, y, light):
        """Store all facts about a light."""
        self.light = light
        self.color = color.black
        self.result_color = self.color
        self.transitioning_to = self.color
        self.step = 0
        self.on = False
        self.increasing = False
        self.x = x
        self.y = y
        self.fade_step = 0

    def update_base(self):
        """Change color depending on where the fish is."""
        if not self.on and random.random() < 0.0005:
            self.transitioning_to = color.RGB(1, 1, 1)
            self.step = abs(self.transitioning_to - self.color) / (FRAME_RATE / 2)
            self.increasing = True
            self.on = True
            self.color = transition(self.color, self.transitioning_to, self.step)
        elif self.on:
            if abs(self.transitioning_to - self.color) <= self.step:
                if self.increasing == True:
                    self.transitioning_to = color.RGB(0, 0, 0)
                    self.step = abs(self.transitioning_to - self.color) / (FRAME_RATE / 2)
                    self.increasing = False
                else:
                    self.increasing = False
                    self.on = False
            self.color = transition(self.color, self.transitioning_to, self.step)

        self.result_color = self.color

    def update_star(self, star):
        coverage = compute_coverage(star, self.x, self.y)
        self.result_color = coverage * star.color + (1 - coverage) * self.result_color

    def blit(self):
        self.light.setRGBRaw(
            *map(lambda x: clamp(0, x, 1), self.result_color.components)
        )




    def fade_to_black(self, steps):
        hue = self.color.to_HSV().h()
        sat = self.color.to_HSV().s()
        value = self.color.to_HSV().v()
        new_color = color.HSV(hue, sat, value - value / steps * self.fade_step).to_RGB()
        self.fade_step += 1
        self.light.setRGBRaw(*map(lambda x: clamp(0, x, 1), new_color.components))


class TwinkleShow(Show):
    def init(self):
        self.lights = [[Light(x, y, self.bridge.get_light(x, y))
                       for y in xrange(self.bridge.HEIGHT)]
                       for x in xrange(self.bridge.WIDTH)]
        #self.sun = Sun()
        self.stars = set()
        self.tick_count = 0
        self.send_event('background music', 'ocean_waves')


    def update(self):
        if self.tick_count > FRAME_RATE * 90:
            fade_time = 3 # sec
            if self.tick_count > FRAME_RATE * 90 + fade_time * FRAME_RATE:
                self.stop()
                return
            for col in self.lights:
                for light in col:
                    light.fade_to_black(fade_time * FRAME_RATE)
        else:
            if random.random() < 0.01:
                self.stars.add(ShootingStar())
            for col in self.lights:
                for light in col:
                    light.update_base()

            for star in list(self.stars):
                if not star.update():
                    self.stars.remove(star)

                for x in xrange(int(star.x), int(star.x+star.width+1)):
                    if x < len(self.lights):
                        for light in self.lights[x]:
                            light.update_star(star)

            for col in self.lights:
                for light in col:
                    light.blit()

        self.tick_count += 1


def compute_coverage(fish, x, y):
    """Calculate the coverage of a fish over a box."""
    middle_of_fish = fish.x + fish.width / 2
    middle_of_panel = x + 0.5
    diff = abs(middle_of_fish - middle_of_panel) / (fish.width / 2)
    scale = 1 - diff
    min_x = min(max(x, fish.x), x + 1)
    max_x = max(min(x+1, fish.x + fish.width), x)
    min_y = min(max(y, fish.y), y + 1)
    max_y = max(min(y+1, fish.y + fish.height), y)
    coverage = (max_x - min_x) * (max_y - min_y)
    return coverage * scale

def transition(start, end, step):
    """Return a vector equal to (end - start) * step + start."""
    if start == end:
        return start
    diff_vector = end - start
    step_vector = diff_vector / abs(diff_vector) * step
    result = step_vector + start
    return result

def clamp(low, x, high):
    """Return v such that low <= v <= high and |x - v| is minimal."""
    return max(low, min(x, high))
