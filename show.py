from __future__ import division, print_function
import time
import Queue

class Clock(object):
    def __init__(self):
        self.last_tick = time.time()

    def tick(self, framerate):
        sec_per_frame = 1.0 / framerate
        diff = time.time() - self.last_tick
        while diff < sec_per_frame:
            diff = time.time() - self.last_tick

        if diff > (1.05 * sec_per_frame):
            print("WARN: tick is not being called fast enough\n",
                  "target {}, actual {}".format(sec_per_frame, diff))
        self.last_tick = time.time()


class Show(object):
    def __init__(self, bridge, send_queue, recv_queue):
        self.bridge = bridge
        self.clock = Clock()
        self.running = True
        self.send_queue = send_queue
        self.recv_queue = recv_queue
        self.init()

    def run(self, framerate=40):
        while self.running:
            self.update()
            self.bridge.update()
            self.clock.tick(framerate)

    def stop(self):
        self.running = False

    def send_event(self, event_name, data=None):
        self.send_queue.put((event_name, data))

    def recv_event(self):
        try:
            msg = self.recv_queue.get_nowait()
            return msg
        except Queue.Empty:
            return None

    def init(self):
        """Override me"""
        raise NotImplementedError

    def update(self):
        """Override me"""
        raise NotImplementedError
